#include <iostream>

#include "cpp-lib/util.h"

int main() { 
  std::cout 
      << cpl::util::json("foo", 123) << std::endl
      << cpl::util::json("bar", "xx '\"") << std::endl
      << cpl::util::json("tab", "space\tis big") << std::endl

      << cpl::util::json("std::string", std::string("im string")) << std::endl
      << cpl::util::json("std::string with Ctrl-C", std::string("imstring")) << std::endl

      << cpl::util::json("pi", 3.14) << std::endl
      << cpl::util::json("minus1", -1) << std::endl
      << cpl::util::json("witch", 0xdeadbeef) << std::endl

      // << cpl::util::json("doesntwork", std::cin) << std::endl

  ;
}
