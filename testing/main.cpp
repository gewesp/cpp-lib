/**
 * Main template
 */

#include <cassert>
#include <cstdlib>
#include <exception>
#include <iostream>
#include <memory>
#include <stdexcept>
#include <string>

#include "cpp-lib/command_line.h"
#include "cpp-lib/util.h"

using namespace cpl::util;

namespace {

const opm_entry options[] = {
    // Boolean option with -l shorthand
    opm_entry("help", opp(false, 'h')),
    // Boolean option with -l shorthand
    opm_entry("list", opp(false, 'l')),
    // Option with one argument
    opm_entry("output", opp(true, 'o')),
};

void usage(std::string const& name) {
  std::cerr << "usage: " << name
            << " [ options ]\n"
               "-h, --help           Output help text.\n"
               "-l, --list           List some values.\n"
               "-o, --output <file>  Write output to <file>.\n";
}

}  // anonymous namespace

int main(int const argc, char const* const* const argv) {
  try {
    if (argc == 1) {
      std::cout << "Type " << argv[0] << " --help for help.\n";
      return 0;
    }

    command_line cl(options, options + size(options), argv);

    if (cl.is_set("help")) {
      usage(argv[0]);
      return 0;
    }

    if (cl.is_set("output")) {
      std::cout << "Write output to " << cl.get_arg("output") << '\n';
    }

  } catch (std::runtime_error const& e) {
    std::cerr << e.what() << '\n';
    return 1;
  }

  return 0;
}
