//
// Repro of an annoying segfault with a bad run_server() API
//

#include <iostream>
#include <stdexcept>
#include <string>

#include <cassert>

#include "cpp-lib/sys/server.h"
#include "cpp-lib/sys/util.h"
#include "cpp-lib/ogn.h"
#include "cpp-lib/util.h"


using namespace cpl::ogn;


bool handle_line(
    std::string const&,
    std::istream&,
    std::ostream&,
    std::ostream&,
    const cpl::util::server_status&) {

  return true;
}

void repro_segfault() {
  // Doesn't happen without the while loop?!!!
  while (true) {

      // The main thread goes on to connect to the glidernet server
      // and continuously updates the database.
      auto const c = cpl::ogn::connect(std::cout);
      c->   send_timeout(60);
      c->receive_timeout(60);

      cpl::util::network::instream is            (*c);
      cpl::util::network::onstream stream_to_aprs(*c);

      cpl::ogn::login(std::cout, stream_to_aprs, is, 
          "cpl-test"
          "CPLOGN");

      std::string line;

      while (cpl::util::getline(is, line, 200, 100)) {

        // Provoke segfault!
        stream_to_aprs << "# We are there" << std::endl;

      } // end while(getline())


    std::cout << "RECONNECT" << std::endl;

  }
}

int main( int , char const* const* const ) {
  {
    cpl::util::server_parameters sp;
    sp.service = "4711";
    sp.server_name = "SERVERNAME";
    sp.background = true;
    sp.log_connections = true;
    sp.n_listen_retries = 600;
    sp.listen_retry_time = 1.0;                                

    cpl::util::running_flag run;

    auto mgr = cpl::util::run_server(
        handle_line,
        run,
        boost::none, // No welcome message
        sp);
  }

  repro_segfault();
}
