#include <iostream>
#include <sstream>
#include <stdexcept>

#include "cpp-lib/sys/syslogger.h"


// A similar constallation causes this in KTrax:
// basic_ios::clear: iostream error; continuing to next line
// See src/modules/liverange/liverange.cpp; line 1643ff
int f(std::ostream& log) {
  std::stringstream ss;

  ss << "Hello world" << std::endl;
  ss << "foo bar" << std::endl;

  for (int i = 1; i < 10; ++i) {
    ss << "The number is " << i << std::endl;
  }

  std::string line;
  while (std::getline(ss, line)) {
    log << cpl::util::log::prio::NOTICE
        << "Replay: RESULT: " << line
        << std::endl;
  }

  return 4711;
}

int main() { 
  cpl::util::log::syslogger sl("TEST");
  try { f(sl); } 
  catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
  }
}
